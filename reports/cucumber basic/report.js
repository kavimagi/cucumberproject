$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/feature/Vendor.feature");
formatter.feature({
  "name": "Login as vendor",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "Open Chrome Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "VendorSearch.OpenBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Set TimeOut",
  "keyword": "And "
});
formatter.match({
  "location": "VendorSearch.TimeOut()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the URL",
  "keyword": "And "
});
formatter.match({
  "location": "VendorSearch.GetURL()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login as vendor",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Enter Email",
  "keyword": "And "
});
formatter.match({
  "location": "VendorSearch.enterEmail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Password",
  "keyword": "And "
});
formatter.match({
  "location": "VendorSearch.passWord()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Login Button",
  "keyword": "When "
});
formatter.match({
  "location": "VendorSearch.clickOn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify Login is successful",
  "keyword": "Then "
});
formatter.match({
  "location": "VendorSearch.success()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on searchVendor",
  "keyword": "And "
});
formatter.match({
  "location": "VendorSearch.searchVendor()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Vendor TaxID",
  "keyword": "And "
});
formatter.match({
  "location": "VendorSearch.searchTaxID()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Print VendorName",
  "keyword": "And "
});
formatter.match({
  "location": "VendorSearch.vendorName()"
});
formatter.result({
  "status": "passed"
});
});