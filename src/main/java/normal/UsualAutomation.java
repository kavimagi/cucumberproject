package normal;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class UsualAutomation {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");
		driver.findElementById("email").sendKeys("kavimagi23@gmail.com");
		driver.findElementById("password").sendKeys("Pa55word");
		driver.findElementById("buttonLogin").click();
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByXPath("//button[text()= ' Vendors']")).perform();
		builder.moveToElement(driver.findElementByLinkText("Search for Vendor")).click().perform();
		driver.findElementByName("vendorTaxID").sendKeys("RO123456");
		driver.findElementById("buttonSearch").click();
		WebElement table = driver.findElementByClassName("table");
		table.findElements(By.tagName("tr"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		
		List<WebElement> column = table.findElements(By.tagName("td"));
		WebElement vendor = column.get(0);
		System.out.println(vendor.getText());
		
		
		
		
		
	}

}
