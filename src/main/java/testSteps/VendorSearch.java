package testSteps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VendorSearch {

	public ChromeDriver driver;
	@Given ("Open Chrome Browser")
	public void OpenBrowser() {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
	}
	
	@And ("Set TimeOut")
	public void TimeOut() {
	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	}
	
	@And ("Enter the URL")
	public void GetURL() {
		driver.get("https://acme-test.uipath.com/account/login");
	}
	
	@And("Enter Email")
	public void enterEmail() {
		driver.findElementById("email").sendKeys("kavimagi23@gmail.com");
		
	}
	
	
	@And("Enter Password")
	public void passWord() {
		driver.findElementById("password").sendKeys("Pa55word");
	}
	
	
	@When("Click on Login Button")
	public void clickOn() {
		driver.findElementById("buttonLogin").click();
	}	
	
	
	@Then("Verify Login is successful")
	public void success() {
		System.out.println("login succesful");
	}	
	
	@And("Click on searchVendor")
	public void searchVendor() {
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByXPath("//button[text()= ' Vendors']")).perform();
		builder.moveToElement(driver.findElementByLinkText("Search for Vendor")).click().perform();	
		
	}	
	
	@And("Enter Vendor TaxID")
	public void searchTaxID() {
		driver.findElementByName("vendorTaxID").sendKeys("RO123456");
		driver.findElementById("buttonSearch").click();

	}	
	
	@And("Print VendorName")
	public void vendorName() {
		WebElement table = driver.findElementByClassName("table");
		table.findElements(By.tagName("tr"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		
		List<WebElement> column = table.findElements(By.tagName("td"));
		WebElement vendor = column.get(0);
		System.out.println(vendor.getText());
		
	}	

	
}
