package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/main/java/feature",glue="testSteps",
monochrome=true,dryRun=false,plugin= {"pretty","html:reports/cucumber basic"})
 
//Snippets=SnippetType.CAMELCASE)

public class TestRun {

}
