Feature: Login as vendor

Background:
Given Open Chrome Browser
And Set TimeOut
And Enter the URL

Scenario: Login as vendor
And Enter Email
And Enter Password
When Click on Login Button
Then Verify Login is successful
And Click on searchVendor
And Enter Vendor TaxID
And Print VendorName
